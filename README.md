Role Name
=========

A role that installs the INCEpTION semantic annotation platform as Docker stack, <https://inception-project.github.io>

Role Variables
--------------

The most important variables are listed below:

``` yaml
inception_project_compose_dir: '/srv/inception_project_stack'
inception_project_docker_stack_name: 'inception-project'
inception_project_docker_service_name: 'inception'
inception_project_docker_image: 'inceptionproject/inception:0.17.3'
inception_project_docker_network: 'inception_project_net'
inception_project_service_port: 8080
# IMPORTANT. Set it to True for the server that is going to host the DB
inception_project_behind_haproxy: True
inception_project_haproxy_public_net: 'haproxy-public'
# Settings
inception_project_server_port: 8080
inception_project_server_address: '0.0.0.0'
inception_project_backup_interval: 300
inception_project_backup_keep_number: 10
# In seconds
inception_project_backup_keep_time: 0
inception_project_server_auth_mode: 'database'
inception_project_server_preauth_header: 'remote_user'
inception_project_server_preauth_default_roles: ''
inception_project_server_preauth_users_roles: []
# - { username: 'user', roles: 'ROLE_PROJECT_CREATOR,ROLE_ADMIN' }
# DB
inception_project_db_as_container: True
inception_project_db_image: 'mariadb:10.5'
inception_project_db_host: 'mysql-server'
inception_project_db_port: 3306
inception_project_db_name: 'inception'
inception_project_db_user: 'inception_user'
#inception_project_db_pwd: 'set it in a vault file'

inception_project_settings:
  - 'debug.showExceptionPage=false'
  - 'user.profile.accessible=true'
  - 'user-selection.hideUsers=false'
  - 'server.port={{ inception_project_server_port }}'
  - 'server.address={{ inception_project_server_address }}'
  - 'server.ajp.port=-1'
  - 'backup.interval={{ inception_project_backup_interval }}'
  - 'backup.keep.number={{ inception_project_backup_keep_number }}'
  - 'backup.keep.time={{ inception_project_backup_keep_time }}'
  - 'auth.mode={{ inception_project_server_auth_mode }}'
  - 'auth.preauth.header.principal={{ inception_project_server_preauth_header }}'
  - 'auth.preauth.newuser.roles={{ inception_project_server_preauth_default_roles }}'
```

Dependencies
------------

A Docker Swarm cluster

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
